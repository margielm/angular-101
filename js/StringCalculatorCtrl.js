angular.module("training", [])
    .factory("calculator", function () {
        return new StringCalculator();
    })
    .controller("StringCalculatorCtrl", function ($scope, calculator) {
        $scope.items = [];

        $scope.changed = function () {
            try {
                $scope.result = calculator.calculate($scope.expression);
            } catch (e) {
                $scope.result = e.message;
            }
        };
        $scope.submit = function () {
            console.log("submitted!")
            $scope.items.push({ expression: $scope.expression, result: $scope.result});

        };

    })


