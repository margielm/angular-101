function StringCalculator() {
    this.calculate = function (expression) {
        if (_.isEmpty(expression)) {
            return 0;
        }
        var elements = split(expression);
        return sum(elements);
    };

    function split(expression) {
        return expression.replace('\n\r', ',').split(',');
    }

    function validate(elementAsInt) {
        if (elementAsInt < 0) {
            throw new Error("Cannot sum negative numbers!");
        }
    }

    function sum(elements) {
        var result = 0;
        for (var i = 0; i < elements.length; i++) {
            result += toIntIfValid(elements[i]);
        }
        return result;
    }

    function toIntIfValid(element) {
        var elementAsInt = parseInt(element);
        validate(elementAsInt);
        return elementAsInt;
    }
}