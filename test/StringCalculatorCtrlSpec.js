describe("StringCalculatorCtrl should", function () {
    var $scope;
    var calculator;

    beforeEach(module("training"));

    beforeEach(inject(function ($rootScope, $controller) {
        $scope = $rootScope;
        calculator = mockCalculator();
        $controller("StringCalculatorCtrl", {$scope: $scope, calculator: calculator})
    }));


    it("pass expression to calculator", function () {
        $scope.expression = 'abc';
        spyOn(calculator, "calculate").andReturn(2);

        $scope.changed();

        expect(calculator.calculate).toHaveBeenCalledWith($scope.expression)
        expect(calculator.calculate.callCount).toBe(1);
        expect($scope.result).toBe(2);
    });

    it("display error message on exception", function () {
        spyOn(calculator, "calculate").andThrow(new Error("error message"));

        $scope.changed();

        expect($scope.result).toBe("error message");
    });

    it("store expression and result into items on submitting", function () {

        submitWith("abc", 2);

        expect($scope.items).toContain({expression: "abc", result: 2});
    });

    it("store two items due to submitting twice", function () {
        submitWith("abc", 2);

        submitWith("abcd", 2);

        expect($scope.items)
            .toContain({expression: "abc", result: 2}, {expression: "abcd", result: 2});
    });

    function mockCalculator() {
        var calculator = function () {
        };
        calculator.calculate = function () {
        };

        return calculator;
    }
    function submitWith(expression, result) {
        $scope.expression = expression;
        $scope.result = result;
        $scope.submit();
    }

})