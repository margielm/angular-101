/*
 # An empty string returns zero
 # A single number returns the value
 # Two numbers, comma delimited, returns the sum
 # Two numbers, newline delimited, returns the sum
 # Negative numbers throw an exception
 # Three numbers, delimited either way, returns the sum
 # Numbers greater than 1000 are ignored
 # A single char delimiter can be defined on the first line (e.g. //# for a ‘#’ as the delimiter)
 # A multi char delimiter can be defined on the first line (e.g. //[###] for ‘###’ as the delimiter)
 # Many single or multi-char delimiters can be defined (each wrapped in square brackets)
 */

describe("StringCalculator should", function () {
    var calculator = new StringCalculator();

    it("return zero for empty string", function () {
        //given

        //when
        var sum = calculator.calculate("");

        //then
        expect(sum).toBe(0);
    });

    it("return 1 for '1'", function () {
        //given

        //when
        var sum = calculator.calculate("1");

        //then
        expect(sum).toBe(1);
    });

    it("return 10 for '10'", function () {
        //given

        //when
        var sum = calculator.calculate("10");

        //then
        expect(sum).toBe(10);
    });

    it("return sum for two numbers delimited by ','", function () {
        //given

        //when
        var sum = calculator.calculate("2,3");

        //then
        expect(sum).toBe(5);
    });

    it("return sum for two numbers delimited by '\\n\\r'", function () {
        //given

        //when
        var sum = calculator.calculate("5\n\r4");

        //then
        expect(sum).toBe(9);
    });

    it("throw exception for negative numbers", function () {
        //given

        //when
        var summing = function () {
            calculator.calculate("-1000")
        };

        //then
        expect(summing).toThrow("Cannot sum negative numbers!");
    });

    it("throw exception for negative second number", function () {
        //given

        //when
        var summing = function () {
            calculator.calculate("1,-900");
        };

        //then
        expect(summing).toThrow("Cannot sum negative numbers!");
    });

    it("return sum for three numbers delimited by ','", function () {
        //given

        //when
        var sum = calculator.calculate("1,2,3");

        //then
        expect(sum).toBe(6);
    });

});